#!/bin/bash

# Configuration
MYSQL_USER="user_sql"
MYSQL_PASSWORD="password_sql"
MYSQL_HOST="localhost"
MYSQL_BACKUP_DIR="/path/to/mysql/backup/directory"

POSTGRES_USER="user_postgres"
POSTGRES_PASSWORD="password_postgres"
POSTGRES_HOST="localhost"
POSTGRES_BACKUP_DIR="/path/to/postgres/backup/directory"

# Function to perform MySQL backup
perform_mysql_backup() {
    local timestamp=$(date +%Y%m%d%H%M%S)
    local filename="${MYSQL_BACKUP_DIR}/mysql_backup_${timestamp}.sql"

    mysqldump --user=${MYSQL_USER} --password=${MYSQL_PASSWORD} --host=${MYSQL_HOST} --all-databases > ${filename}

    echo "MySQL backup created: ${filename}"
}

# Function to perform PostgreSQL backup
perform_postgresql_backup() {
    local timestamp=$(date +%Y%m%d%H%M%S)
    local filename="${POSTGRES_BACKUP_DIR}/postgresql_backup_${timestamp}.sql"

    pg_dumpall -U ${POSTGRES_USER} -h ${POSTGRES_HOST} -f ${filename}

    echo "PostgreSQL backup created: ${filename}"
}

# Main script
main() {
    # Create backup directories if they don't exist
    mkdir -p ${MYSQL_BACKUP_DIR}
    mkdir -p ${POSTGRES_BACKUP_DIR}

    # Perform backups
    perform_mysql_backup
    perform_postgresql_backup
}

# Execute the main script
main
